package com.commit451.gitlab.api

import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import java.io.IOException

/**
 * we have some issues with parsing XML that contains HTML tags, like <br/>. So,
 * we remove them at the request layer.
 */
class FixBrokenXmlRequestInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val response = chain.proceed(request)
        return if (request.url.pathSegments.last().contains(".atom")) {
            val newBody = response.body?.string()

            response.newBuilder()
                .body(
                    newBody?.replace(Regex("<br\\s*/?>"), "")
                        ?.toResponseBody()
                )
                .build()
        } else {
            response
        }
    }
}