package com.commit451.gitlab.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import coil.load
import com.commit451.gitlab.App
import com.commit451.gitlab.R
import com.commit451.gitlab.databinding.ActivityFullscreenImageBinding
import com.commit451.gitlab.model.api.Project

/**
 * A full-screen activity that opens the clicked images
 */
class FullscreenImageActivity : BaseActivity() {

    companion object {

        private const val KEY_URL = "url"
        private const val KEY_PROJECT = "project"

        fun newIntent(context: Context, project: Project, url: String): Intent {
            val intent = Intent(context, FullscreenImageActivity::class.java)
            intent.putExtra(KEY_PROJECT, project)
            intent.putExtra(KEY_URL, url)
            return intent
        }
    }

    private lateinit var binding: ActivityFullscreenImageBinding

    private lateinit var project: Project

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFullscreenImageBinding.inflate(layoutInflater)
        setContentView(binding.root)

        project = intent.getParcelableExtra(KEY_PROJECT)!!

        binding.toolbar.setNavigationIcon(R.drawable.ic_back_24dp)
        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        binding.photoView.scaleType = ImageView.ScaleType.FIT_CENTER

        var imageUrl: String = intent.getStringExtra(KEY_URL)!!
        if (imageUrl.startsWith("/")) {
            imageUrl =
                App.get().getAccount().serverUrl.toString() + project.pathWithNamespace + imageUrl
        }
        binding.photoView.load(imageUrl)
    }
}
