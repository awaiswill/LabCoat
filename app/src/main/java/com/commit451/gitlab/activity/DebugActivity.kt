package com.commit451.gitlab.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.commit451.gitlab.R
import com.commit451.gitlab.databinding.ActivityDebugBinding

/**
 * Allows some debugging
 */
class DebugActivity : BaseActivity() {

    companion object {

        fun newIntent(context: Context): Intent {
            return Intent(context, DebugActivity::class.java)
        }
    }

    private lateinit var binding: ActivityDebugBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDebugBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.title = "Here Be Dragons"
        binding.toolbar.setNavigationIcon(R.drawable.ic_back_24dp)
        binding.toolbar.setNavigationOnClickListener { onBackPressed() }
    }
}
